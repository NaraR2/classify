package com.example.androidfinal.data;

import android.content.Context;

import com.example.androidfinal.data.daos.AccountDao;
import com.example.androidfinal.data.daos.CategoryDao;
import com.example.androidfinal.data.daos.OperationDao;
import com.example.androidfinal.data.entities.Account;
import com.example.androidfinal.data.entities.Category;
import com.example.androidfinal.data.entities.Operation;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Account.class, Operation.class, Category.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract AccountDao accountDao();

    public abstract OperationDao operationDao();

    public abstract CategoryDao categoryDao();


    private static AppDatabase appDatabase;

    public static AppDatabase getInstance(Context context) {
        if (null == appDatabase) {
            appDatabase = buildDatabaseInstance(context);
        }
        return appDatabase;
    }

    private static AppDatabase buildDatabaseInstance(Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "app-database")
                .build();
    }

    public void cleanUp() {
        appDatabase = null;
    }
}