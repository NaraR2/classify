package com.example.androidfinal.data.entities;

import java.io.Serializable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Category implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public String title;

    public Category(String title){
        this.title = title;
    }
}
