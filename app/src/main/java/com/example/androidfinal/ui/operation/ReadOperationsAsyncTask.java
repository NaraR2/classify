package com.example.androidfinal.ui.operation;

import android.os.AsyncTask;
import android.view.View;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.data.entities.Operation;

import java.lang.ref.WeakReference;
import java.util.List;

import androidx.fragment.app.FragmentActivity;

public class ReadOperationsAsyncTask extends AsyncTask<Void,Void,List<Operation>> {

    private WeakReference<FragmentActivity> activityReference;
    private WeakReference<OperationListFragment> fragmentReference;
    private int accountId;

    ReadOperationsAsyncTask(FragmentActivity activity, OperationListFragment fragment, int accountId) {
        activityReference = new WeakReference<>(activity);
        fragmentReference = new WeakReference<>(fragment);
        this.accountId = accountId;
    }

    @Override
    protected List<Operation> doInBackground(Void... voids) {
        if (activityReference.get() != null){
            if(accountId != 0)
                return ((MainActivity)activityReference.get()).appDatabase.operationDao().findAllForAccount(accountId);
            else
                return ((MainActivity)activityReference.get()).appDatabase.operationDao().getAll();
        }
        else
            return null;
    }

    @Override
    protected void onPostExecute(List<Operation> operations) {
        if (operations != null && operations.size() > 0 ){
            fragmentReference.get().operations = operations;

            // hides empty text view
            fragmentReference.get().textViewMsgEmptyList.setVisibility(View.GONE);

            // create and set the adapter on RecyclerView instance to display list
            fragmentReference.get().operationsAdapter = new OperationsAdapter(operations, fragmentReference.get());
            fragmentReference.get().recyclerView.setAdapter(fragmentReference.get().operationsAdapter);
        }
    }
}
