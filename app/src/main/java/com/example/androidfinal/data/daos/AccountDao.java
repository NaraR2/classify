package com.example.androidfinal.data.daos;

import com.example.androidfinal.data.entities.Account;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface AccountDao {

    @Query("SELECT * FROM Account")
    List<Account> getAll();

    @Query("SELECT * FROM Account WHERE id IN (:accountIds)")
    List<Account> loadAllByIds(int[] accountIds);

    @Query("SELECT * FROM Account WHERE id LIKE :id LIMIT 1")
    Account findById(int id);

    @Query("SELECT * FROM Account WHERE title LIKE :title LIMIT 1")
    Account findByTitle(String title);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Account... accounts);

    @Delete
    void delete(Account account);
}
