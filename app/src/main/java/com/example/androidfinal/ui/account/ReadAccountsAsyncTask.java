package com.example.androidfinal.ui.account;

import android.os.AsyncTask;
import android.view.View;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.data.entities.Account;

import java.lang.ref.WeakReference;
import java.util.List;

import androidx.fragment.app.FragmentActivity;

public class ReadAccountsAsyncTask extends AsyncTask<Void,Void,List<Account>> {

    private WeakReference<FragmentActivity> activityReference;
    private WeakReference<AccountListFragment> fragmentReference;

    ReadAccountsAsyncTask(FragmentActivity activity, AccountListFragment fragment) {
        activityReference = new WeakReference<>(activity);
        fragmentReference = new WeakReference<>(fragment);
    }

    @Override
    protected List<Account> doInBackground(Void... voids) {
        if (activityReference.get()!=null)
            return ((MainActivity)activityReference.get()).appDatabase.accountDao().getAll();
        else
            return null;
    }

    @Override
    protected void onPostExecute(List<Account> accounts) {
        if (accounts != null && accounts.size() > 0 ){
            fragmentReference.get().accounts = accounts;

            // hides empty text view
            fragmentReference.get().textViewMsgEmptyList.setVisibility(View.GONE);

            // create and set the adapter on RecyclerView instance to display list
            fragmentReference.get().accountsAdapter = new AccountsAdapter(accounts, fragmentReference.get());
            fragmentReference.get().recyclerView.setAdapter(fragmentReference.get().accountsAdapter);
        }
    }
}
