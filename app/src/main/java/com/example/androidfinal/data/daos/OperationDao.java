package com.example.androidfinal.data.daos;

import com.example.androidfinal.data.entities.Account;
import com.example.androidfinal.data.entities.Operation;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface OperationDao {

    @Query("SELECT * FROM Operation")
    List<Operation> getAll();

    @Query("SELECT * FROM Operation WHERE id IN (:operationIds)")
    List<Operation> loadAllByIds(int[] operationIds);

    @Query("SELECT * FROM Operation WHERE accountId=:accountId")
    List<Operation> findAllForAccount(int accountId);

    @Query("SELECT * FROM Operation WHERE categoryId=:categoryId")
    List<Operation> findAllForCategory(int categoryId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Operation... operations);

    @Delete
    void delete(Operation operation);
}
