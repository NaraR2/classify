package com.example.androidfinal.ui.operation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Operation;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class OperationsAdapter extends RecyclerView.Adapter<OperationsAdapter.OperationHolder> {

    private Fragment fragmentContext;
    private LayoutInflater layoutInflater;
    private OnOperationItemClick onOperationItemClick;
    private List<Operation> list;

    OperationsAdapter(List<Operation> list, Fragment fragmentContext) {
        this.fragmentContext = fragmentContext;
        layoutInflater = LayoutInflater.from(this.fragmentContext.getContext());
        this.onOperationItemClick = (OnOperationItemClick) this.fragmentContext;
        this.list = list;
    }

    @Override
    public OperationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.operation_list_item, parent, false);
        return new OperationHolder(view);
    }

    @Override
    public void onBindViewHolder(OperationHolder holder, int position) {
        Log.e("bind", "onBindViewHolder: " + list.get(position));
        holder.textViewAmount.setText(Double.toString(list.get(position).amount));
        holder.textViewConcept.setText(list.get(position).concept);
        holder.textViewDate.setText(list.get(position).date.toString());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class OperationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewAmount;
        TextView textViewConcept;
        TextView textViewDate;

        OperationHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textViewAmount = itemView.findViewById(R.id.item_amount);
            textViewConcept = itemView.findViewById(R.id.item_concept);
            textViewDate = itemView.findViewById(R.id.item_date);
        }

        @Override
        public void onClick(View view) {
            onOperationItemClick.onOperationClick(getAdapterPosition());
        }
    }

    public interface OnOperationItemClick {
        void onOperationClick(int pos);
    }
}