package com.example.androidfinal.ui.category;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Category;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryHolder> {

    private Fragment fragmentContext;
    private LayoutInflater layoutInflater;
    private OnCategoryItemClick onCategoryItemClick;
    private List<Category> list;

    CategoriesAdapter(List<Category> list, Fragment fragmentContext) {
        this.fragmentContext = fragmentContext;
        layoutInflater = LayoutInflater.from(this.fragmentContext.getContext());
        this.onCategoryItemClick = (OnCategoryItemClick) this.fragmentContext;
        this.list = list;
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.category_list_item, parent, false);
        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        Log.e("bind", "onBindViewHolder: " + list.get(position));
        holder.textViewTitle.setText(list.get(position).title);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTitle;

        CategoryHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textViewTitle = itemView.findViewById(R.id.item_title);
        }

        @Override
        public void onClick(View view) {
            onCategoryItemClick.onCategoryClick(getAdapterPosition());
        }
    }

    public interface OnCategoryItemClick {
        void onCategoryClick(int pos);
    }
}