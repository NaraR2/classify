package com.example.androidfinal.data.daos;

import com.example.androidfinal.data.entities.AccountWithOperations;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

@Dao
public interface AccountWithOperationsDao {

    @Transaction
    @Query("SELECT * FROM Account")
    public List<AccountWithOperations> getAccountWithOperations();
}
