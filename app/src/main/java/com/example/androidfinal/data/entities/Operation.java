package com.example.androidfinal.data.entities;

import com.example.androidfinal.data.helpers.DateConverter;

import java.io.Serializable;
import java.util.Date;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity
public class Operation implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public double amount;

    public String concept;

    @TypeConverters(DateConverter.class)
    public Date date;

    /*relationships*/
    public int accountId;

    public int categoryId;

    public Operation(int accountId, double amount, String concept, Date date, int categoryId){
        this.accountId = accountId;
        this.amount = amount;
        this.concept = concept;
        this.date = date;
        this.categoryId = categoryId;
    }

    public void update(double amount, String concept, Date date, int categoryId){
        this.amount = amount;
        this.concept = concept;
        this.date = date;
        this.categoryId = categoryId;
    }
}
