package com.example.androidfinal.ui.home;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.data.entities.Category;
import com.example.androidfinal.data.entities.Operation;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.model.PieChartData;

public class CalculateChartAsyncTask extends AsyncTask<Void, Void, List<Operation>> {

    //Prevent leak
    private WeakReference<FragmentActivity> activityReference;
    private WeakReference<HomeFragment> fragmentReference;
    private int accountId;
    private List<Category> categories;

    CalculateChartAsyncTask(FragmentActivity activity, HomeFragment fragment) {
        activityReference = new WeakReference<>(activity);
        fragmentReference = new WeakReference<>(fragment);
        this.accountId = fragmentReference.get().account.id;
        this.categories = fragmentReference.get().categories;
    }

    @Override
    protected List<Operation> doInBackground(Void... params) {
        if (activityReference.get() != null){
            return ((MainActivity)activityReference.get()).appDatabase.operationDao().findAllForAccount(accountId);
        }
        else
            return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onPostExecute(List<Operation> operations) {
        if(operations != null && operations.size() > 0 && categories != null && categories.size() > 0){
            //group operations by categories and calculate balance
            HashMap<String, Double> amountByCategory = new HashMap<>();

            int totalAmount = 0;

            for (Operation operation: operations) {
                totalAmount += operation.amount;

                //get category from categoryId in operation
                Optional<Category> operationCategory = this.categories.stream().filter(c -> c.id == operation.categoryId).findFirst();
                String operationCategoryTitle = operationCategory.get().title;

                //update amountsByCategory
                if(!amountByCategory.containsKey(operationCategoryTitle))
                    amountByCategory.put(operationCategoryTitle, operation.amount);
                else {
                    Double newCategoryAmount = amountByCategory.get(operationCategoryTitle) + operation.amount;
                    amountByCategory.remove(operationCategoryTitle);
                    amountByCategory.put(operationCategoryTitle, newCategoryAmount);
                }

                //Set up PieChartData
                List<SliceValue> pieData = new ArrayList<>();
                for (String category: amountByCategory.keySet()) {
                    //Create random colors for categories
                    Random rand = new Random();
                    int r = rand.nextInt(255);
                    int g = rand.nextInt(255);
                    int b = rand.nextInt(255);
                    pieData.add(
                            new SliceValue((amountByCategory.get(category).floatValue()*100)/totalAmount, Color.rgb(r,g,b))
                                    .setLabel(category + ": " + amountByCategory.get(category)));
                }

                PieChartData pieChartData = new PieChartData(pieData);

                //Customize pieChart
                pieChartData.setHasLabels(true);
                pieChartData.setHasLabels(true).setValueLabelTextSize(14);
                pieChartData.setHasCenterCircle(true)
                        .setCenterText1("Total: " + totalAmount)
                        .setCenterText1FontSize(40)
                        .setCenterText1Color(Color.parseColor("#0097A7"));

                fragmentReference.get().pieChartView.setPieChartData(pieChartData);

                //hide no-data text and show chart
                fragmentReference.get().textViewMsgNoData.setVisibility(View.GONE);
                fragmentReference.get().pieChartView.setVisibility(View.VISIBLE);
            }
        }
        else {
            //hide chart and show no-data text
            fragmentReference.get().textViewMsgNoData.setVisibility(View.VISIBLE);
            fragmentReference.get().pieChartView.setVisibility(View.GONE);
        }
    }
}
