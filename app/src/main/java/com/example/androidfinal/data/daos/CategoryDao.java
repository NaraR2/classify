package com.example.androidfinal.data.daos;

import com.example.androidfinal.data.entities.Account;
import com.example.androidfinal.data.entities.Category;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface CategoryDao {

    @Query("SELECT * FROM Category")
    List<Category> getAll();

    @Query("SELECT * FROM Category WHERE id IN (:categoryIds)")
    List<Category> loadAllByIds(int[] categoryIds);

    @Query("SELECT * FROM Category WHERE title LIKE :title LIMIT 1")
    Category findByTitle(String title);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Category... categories);

    @Delete
    void delete(Category category);
}
