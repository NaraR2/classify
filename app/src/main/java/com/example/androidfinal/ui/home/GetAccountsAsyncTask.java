package com.example.androidfinal.ui.home;

import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.widget.ArrayAdapter;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.data.entities.Account;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;

public class GetAccountsAsyncTask extends AsyncTask<Void,Void,List<Account>> {

    private WeakReference<FragmentActivity> activityReference;
    private WeakReference<HomeFragment> fragmentReference;

    public GetAccountsAsyncTask(FragmentActivity activity, HomeFragment fragment) {
        activityReference = new WeakReference<>(activity);
        fragmentReference = new WeakReference<>(fragment);
    }

    @Override
    protected List<Account> doInBackground(Void... voids) {
        if (activityReference.get()!=null)
            return ((MainActivity)activityReference.get()).appDatabase.accountDao().getAll();
        else
            return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onPostExecute(List<Account> accounts) {
        if (accounts != null && accounts.size() > 0 ){
            fragmentReference.get().accounts = accounts;
            fragmentReference.get().account = accounts.get(0);

            List<String> accountNames = accounts.stream().map(sc -> sc.title).collect(Collectors.toList());
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(
                    activityReference.get(), android.R.layout.simple_spinner_dropdown_item, accountNames);
            fragmentReference.get().spinnerAccounts.setAdapter(spinnerAdapter);

            fragmentReference.get().textViewMsgWelcome.setVisibility(View.GONE);
            fragmentReference.get().spinnerAccountsLabel.setVisibility(View.VISIBLE);
            fragmentReference.get().spinnerAccounts.setVisibility(View.VISIBLE);

            new GetCategoriesAsyncTask(activityReference.get(), fragmentReference.get()).execute();
        }
        else{
            fragmentReference.get().textViewMsgWelcome.setVisibility(View.VISIBLE);
            fragmentReference.get().spinnerAccountsLabel.setVisibility(View.GONE);
            fragmentReference.get().spinnerAccounts.setVisibility(View.GONE);
            fragmentReference.get().textViewMsgNoData.setVisibility(View.GONE);
            fragmentReference.get().accountOperationsBtn.setVisibility(View.GONE);
        }
    }
}
