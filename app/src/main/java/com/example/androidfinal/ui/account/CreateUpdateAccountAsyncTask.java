package com.example.androidfinal.ui.account;

import android.os.AsyncTask;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.data.entities.Account;

import java.lang.ref.WeakReference;

import androidx.fragment.app.FragmentActivity;
import androidx.navigation.fragment.NavHostFragment;

public class CreateUpdateAccountAsyncTask extends AsyncTask<Void,Void,Boolean> {

    private WeakReference<FragmentActivity> activityReference;
    private WeakReference<AccountFragment> fragmentReference;
    private Account account;

    // only retain a weak reference to the activity
    CreateUpdateAccountAsyncTask(FragmentActivity context, AccountFragment fragment, Account account) {
        activityReference = new WeakReference<>(context);
        fragmentReference = new WeakReference<>(fragment);
        this.account = account;
    }

    // doInBackground methods runs on a worker thread
    @Override
    protected Boolean doInBackground(Void... voids) {
        ((MainActivity)activityReference.get()).appDatabase.accountDao().insertAll(account);
        return true;
    }

    // onPostExecute runs on main thread
    @Override
    protected void onPostExecute(Boolean bool) {
        NavHostFragment.findNavController(fragmentReference.get()).navigateUp();
    }
}
