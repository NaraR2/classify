package com.example.androidfinal.data.entities;

import androidx.room.Embedded;
import androidx.room.Relation;

public class OperationAndCategory {

    @Embedded
    public Operation operation;

    @Relation(parentColumn = "id", entityColumn = "categoryId")
    public Category category;
}
