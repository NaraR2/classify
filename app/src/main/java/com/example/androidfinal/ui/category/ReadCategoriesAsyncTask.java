package com.example.androidfinal.ui.category;

import android.os.AsyncTask;
import android.view.View;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.data.entities.Category;

import java.lang.ref.WeakReference;
import java.util.List;

import androidx.fragment.app.FragmentActivity;

public class ReadCategoriesAsyncTask extends AsyncTask<Void,Void,List<Category>> {

    private WeakReference<FragmentActivity> activityReference;
    private WeakReference<CategoryListFragment> fragmentReference;

    ReadCategoriesAsyncTask(FragmentActivity activity, CategoryListFragment fragment) {
        activityReference = new WeakReference<>(activity);
        fragmentReference = new WeakReference<>(fragment);
    }

    @Override
    protected List<Category> doInBackground(Void... voids) {
        if (activityReference.get()!=null)
            return ((MainActivity)activityReference.get()).appDatabase.categoryDao().getAll();
        else
            return null;
    }

    @Override
    protected void onPostExecute(List<Category> categories) {
        if (categories != null && categories.size() > 0 ){
            fragmentReference.get().categories = categories;

            // hides empty text view
            fragmentReference.get().textViewMsgEmptyList.setVisibility(View.GONE);

            // create and set the adapter on RecyclerView instance to display list
            fragmentReference.get().categoriesAdapter = new CategoriesAdapter(categories, fragmentReference.get());
            fragmentReference.get().recyclerView.setAdapter(fragmentReference.get().categoriesAdapter);
        }
    }
}
