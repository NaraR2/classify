package com.example.androidfinal.ui.home;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Account;
import com.example.androidfinal.data.entities.Category;

import java.util.List;
import java.util.Optional;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import lecho.lib.hellocharts.view.PieChartView;

public class HomeFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    List<Account> accounts;
    Spinner spinnerAccounts;
    Account account;
    List<Category> categories;
    TextView spinnerAccountsLabel;
    TextView textViewMsgWelcome;
    TextView textViewMsgNoData;
    PieChartView pieChartView;
    Button accountOperationsBtn;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        //layout, messages, buttons and pie chart
        textViewMsgWelcome = (TextView) getView().findViewById(R.id.msg_welcome);
        textViewMsgNoData = (TextView) getView().findViewById(R.id.msg_no_data);
        spinnerAccountsLabel = (TextView) getView().findViewById(R.id.accounts_spinner_label);
        spinnerAccounts = getView().findViewById(R.id.accounts_spinner);
        pieChartView = getView().findViewById(R.id.chart);
        accountOperationsBtn = (Button) getView().findViewById(R.id.account_operations);

        // accounts
        spinnerAccounts.setOnItemSelectedListener(this);
        new GetAccountsAsyncTask(requireActivity(), this).execute();

        //button to go to operations
        accountOperationsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("account", account);
                bundle.putSerializable("categories", categories.toArray());
                NavHostFragment.findNavController(HomeFragment.this)
                        .navigate(R.id.action_nav_home_to_nav_operations, bundle);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedAccountTitle = parent.getItemAtPosition(position).toString();
        Optional<Account> selectedAccount = accounts.stream().filter(a -> a.title.equals(selectedAccountTitle)).findFirst();
        account = selectedAccount.get();
        new CalculateChartAsyncTask(requireActivity(), this).execute();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
