package com.example.androidfinal.data.entities;

import java.util.List;

import androidx.room.Embedded;
import androidx.room.Relation;

public class AccountWithOperations {

    @Embedded
    public Account account;

    @Relation(parentColumn = "id", entityColumn = "accountId")
    public List<Operation> operations;
}
