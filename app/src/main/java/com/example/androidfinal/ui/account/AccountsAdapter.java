package com.example.androidfinal.ui.account;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Account;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.AccountHolder> {

    private Fragment fragmentContext;
    private LayoutInflater layoutInflater;
    private OnAccountItemClick onAccountItemClick;
    private List<Account> list;

    public AccountsAdapter(List<Account> list, Fragment fragmentContext) {
        this.fragmentContext = fragmentContext;
        layoutInflater = LayoutInflater.from(this.fragmentContext.getContext());
        this.onAccountItemClick = (OnAccountItemClick) this.fragmentContext;
        this.list = list;
    }

    @Override
    public AccountHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.account_list_item, parent, false);
        return new AccountHolder(view);
    }

    @Override
    public void onBindViewHolder(AccountHolder holder, int position) {
        Log.e("bind", "onBindViewHolder: " + list.get(position));
        holder.textViewTitle.setText(list.get(position).title);
        holder.textViewBalance.setText(Double.toString(list.get(position).balance));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class AccountHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewTitle;
        TextView textViewBalance;

        public AccountHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textViewTitle = itemView.findViewById(R.id.item_title);
            textViewBalance = itemView.findViewById(R.id.item_balance);
        }

        @Override
        public void onClick(View view) {
            onAccountItemClick.onAccountClick(getAdapterPosition());
        }
    }

    public interface OnAccountItemClick {
        void onAccountClick(int pos);
    }
}