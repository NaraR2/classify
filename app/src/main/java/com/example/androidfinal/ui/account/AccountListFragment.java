package com.example.androidfinal.ui.account;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Account;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AccountListFragment extends Fragment implements AccountsAdapter.OnAccountItemClick {

    AccountsAdapter accountsAdapter;
    TextView textViewMsgEmptyList;
    RecyclerView recyclerView;
    List<Account> accounts;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {

        textViewMsgEmptyList = (TextView) getView().findViewById(R.id.msg_empty_list);

        FloatingActionButton fab = getView().findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(AccountListFragment.this)
                        .navigate(R.id.action_nav_accounts_to_account_details);
            }
        });

        recyclerView = getView().findViewById(R.id.account_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));

        accounts = new ArrayList<>();
        accountsAdapter = new AccountsAdapter(accounts, this);
        recyclerView.setAdapter(accountsAdapter);

        // fetch list of accounts in background thread
        new ReadAccountsAsyncTask(requireActivity(), this).execute();
    }

    @Override
    public void onAccountClick(final int pos) {
        new AlertDialog.Builder(requireActivity())
                .setTitle("Select Options")
                .setItems(new String[]{"Update"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("account", accounts.get(pos));
                                NavHostFragment.findNavController(AccountListFragment.this)
                                        .navigate(R.id.action_nav_accounts_to_account_details, bundle);
                                break;
                        }
                    }
                }).show();
    }
}