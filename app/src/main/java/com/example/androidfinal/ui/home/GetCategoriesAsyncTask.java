package com.example.androidfinal.ui.home;

import android.os.AsyncTask;
import android.os.Build;
import android.view.View;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.data.entities.Category;

import java.lang.ref.WeakReference;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;

public class GetCategoriesAsyncTask extends AsyncTask<Void,Void, List<Category>> {

    private WeakReference<FragmentActivity> activityReference;
    private WeakReference<HomeFragment> fragmentReference;

    GetCategoriesAsyncTask(FragmentActivity activity, HomeFragment fragment) {
        activityReference = new WeakReference<>(activity);
        fragmentReference = new WeakReference<>(fragment);
    }

    @Override
    protected List<Category> doInBackground(Void... voids) {
        if (activityReference.get()!=null)
            return ((MainActivity)activityReference.get()).appDatabase.categoryDao().getAll();
        else
            return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onPostExecute(List<Category> categories) {
        if (categories != null && categories.size() > 0 ){
            fragmentReference.get().categories = categories;

            fragmentReference.get().accountOperationsBtn.setVisibility(View.VISIBLE);

            //once categories are retrieved the chart needs to be built
            new CalculateChartAsyncTask(activityReference.get(), fragmentReference.get()).execute();
        }
        else {
            fragmentReference.get().accountOperationsBtn.setVisibility(View.GONE);
        }
    }
}
