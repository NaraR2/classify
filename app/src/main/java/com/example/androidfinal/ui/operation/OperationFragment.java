package com.example.androidfinal.ui.operation;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Account;
import com.example.androidfinal.data.entities.Category;
import com.example.androidfinal.data.entities.Operation;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

public class OperationFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private String parameterKey = "operation";
    protected Operation operation; //available for edition
    Account account;
    List<Category> categories;
    Spinner spinnerCategories;
    Category category;
    TextView textViewAccountTitle;
    private TextInputEditText et_amount, et_concept, et_date;
    Calendar calendar;
    DatePickerDialog datePickerDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        try {
            operation = (Operation) getArguments().getSerializable(parameterKey);
            account = (Account) getArguments().getSerializable("account");
            categories = new ArrayList<>();
            Object[] categoriesObjs = (Object[]) getArguments().getSerializable("categories");
            for(Object categoryObj: categoriesObjs){
                categories.add((Category) categoryObj);
            }
        }
        catch (Exception e) {

        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_operation, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {

        //account
        textViewAccountTitle = (TextView) getView().findViewById(R.id.account_title);
        if(account != null)
            textViewAccountTitle.setText(account.title);

        // categories
        spinnerCategories = getView().findViewById(R.id.categories_spinner);
        spinnerCategories.setOnItemSelectedListener(this);
        List<String> categoryNames = categories.stream().map(c -> c.title).collect(Collectors.toList());
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(
                getActivity(), android.R.layout.simple_spinner_dropdown_item, categoryNames);
        spinnerCategories.setAdapter(spinnerAdapter);

        et_amount = getView().findViewById(R.id.et_amount);
        et_concept = getView().findViewById(R.id.et_concept);
        et_date = getView().findViewById(R.id.et_date);

        if (operation != null){
            et_amount.setText(Double.toString(operation.amount));
            et_concept.setText(operation.concept);
            et_date.setText(operation.date.toString());
        }

        Button button = getView().findViewById(R.id.but_save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (operation == null){
                    // fetch data and create object
                    operation = new Operation(
                            account.id,
                            Double.parseDouble(et_amount.getText().toString()),
                            et_concept.getText().toString(),
                            new Date(et_date.getText().toString()),
                            category.id);
                }
                else {
                    // get new data and update object
                    operation.update(
                            Double.parseDouble(et_amount.getText().toString()),
                            et_concept.getText().toString(),
                            new Date(et_date.getText().toString()),
                            category.id);
                }

                //save to db
                new CreateUpdateOperationAsyncTask(requireActivity(), OperationFragment.this, operation).execute();
            }
        });

        TextInputEditText dateTextInput = getView().findViewById(R.id.et_date);
        dateTextInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(calendar == null){
                    calendar = Calendar.getInstance();
                }
                // date picker dialog
                datePickerDialog = new DatePickerDialog(requireActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int day) {
                                dateTextInput.setText((month + 1) + "/" + day + "/" + year);
                                calendar.set(year, month, day);
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedCategoryTitle = parent.getItemAtPosition(position).toString();
        Optional<Category> selectedCategory = categories.stream().filter(a -> a.title.equals(selectedCategoryTitle)).findFirst();
        category = selectedCategory.get();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
