package com.example.androidfinal.data.daos;

import com.example.androidfinal.data.entities.OperationAndCategory;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

@Dao
public interface OperationAndCategoryDao {

    @Transaction
    @Query("SELECT * FROM Operation")
    public List<OperationAndCategory> getOperationsAndCategories();
}
