package com.example.androidfinal.ui.operation;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Account;
import com.example.androidfinal.data.entities.Category;
import com.example.androidfinal.data.entities.Operation;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class OperationListFragment extends Fragment implements OperationsAdapter.OnOperationItemClick {

    Account account;
    List<Category> categories;
    OperationsAdapter operationsAdapter;
    TextView textViewMsgEmptyList;
    TextView textViewAccountTitle;
    RecyclerView recyclerView;
    List<Operation> operations;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        try {
            account = (Account) getArguments().getSerializable("account");
            categories = new ArrayList<>();
            Object[] categoriesObjs = (Object[]) getArguments().getSerializable("categories");
            for(Object categoryObj: categoriesObjs){
                categories.add((Category) categoryObj);
            }
        }
        catch (Exception e) {

        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_operation_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {

        textViewMsgEmptyList = (TextView) getView().findViewById(R.id.msg_empty_list);
        textViewAccountTitle = (TextView) getView().findViewById(R.id.account_title);

        if(account != null)
            textViewAccountTitle.setText(account.title);

        FloatingActionButton fab = getView().findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("account", account);
                bundle.putSerializable("categories", categories.toArray());
                NavHostFragment.findNavController(OperationListFragment.this)
                        .navigate(R.id.action_nav_operations_to_operation_details, bundle);
            }
        });

        recyclerView = getView().findViewById(R.id.operation_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));

        operations = new ArrayList<>();
        operationsAdapter = new OperationsAdapter(operations, this);
        recyclerView.setAdapter(operationsAdapter);

        // fetch list of items in background thread
        new ReadOperationsAsyncTask(requireActivity(), this, account.id).execute();
    }

    @Override
    public void onOperationClick(final int pos) {
        new AlertDialog.Builder(requireActivity())
                .setTitle("Select Options")
                .setItems(new String[]{"Update"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("operation", operations.get(pos));
                                bundle.putSerializable("account", account);
                                bundle.putSerializable("categories", categories.toArray());
                                NavHostFragment.findNavController(OperationListFragment.this)
                                        .navigate(R.id.action_nav_operations_to_operation_details, bundle);
                                break;
                        }
                    }
                }).show();
    }
}