package com.example.androidfinal.data.entities;

import java.io.Serializable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Account implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public String title;

    public double balance;

    public Account(String title){
        this.title = title;
        this.balance = 0;
    }
}
