package com.example.androidfinal.ui.category;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Category;
import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class CategoryFragment extends Fragment {

    private String parameterKey = "category";
    protected Category category;
    private TextInputEditText et_title;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        try {
            category = (Category) getArguments().getSerializable(parameterKey);
        }
        catch (Exception e) {

        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {

        et_title = getView().findViewById(R.id.et_title);
        if (category != null)
            et_title.setText(category.title);

        Button button = getView().findViewById(R.id.but_save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (category == null){
                    // fetch data and create object
                    category = new Category(et_title.getText().toString());
                }
                else {
                    // get new data and update object
                    category.title = et_title.getText().toString();
                }

                //save to db
                new CreateUpdateCategoryAsyncTask(requireActivity(), CategoryFragment.this, category).execute();
            }
        });
    }
}
