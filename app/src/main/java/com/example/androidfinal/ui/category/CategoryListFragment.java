package com.example.androidfinal.ui.category;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Category;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CategoryListFragment extends Fragment implements CategoriesAdapter.OnCategoryItemClick {

    CategoriesAdapter categoriesAdapter;
    TextView textViewMsgEmptyList;
    RecyclerView recyclerView;
    List<Category> categories;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {

        textViewMsgEmptyList = (TextView) getView().findViewById(R.id.msg_empty_list);

        FloatingActionButton fab = getView().findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(CategoryListFragment.this)
                        .navigate(R.id.action_nav_categories_to_category_details);
            }
        });

        recyclerView = getView().findViewById(R.id.category_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));

        categories = new ArrayList<>();
        categoriesAdapter = new CategoriesAdapter(categories, this);
        recyclerView.setAdapter(categoriesAdapter);

        // fetch list of items in background thread
        new ReadCategoriesAsyncTask(requireActivity(), this).execute();
    }

    @Override
    public void onCategoryClick(final int pos) {
        new AlertDialog.Builder(requireActivity())
                .setTitle("Select Options")
                .setItems(new String[]{"Update"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("category", categories.get(pos));
                                NavHostFragment.findNavController(CategoryListFragment.this)
                                        .navigate(R.id.action_nav_categories_to_category_details, bundle);
                                break;
                        }
                    }
                }).show();
    }
}