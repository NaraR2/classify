package com.example.androidfinal.ui.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.androidfinal.R;
import com.example.androidfinal.data.entities.Account;
import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class AccountFragment extends Fragment {

    protected Account account;
    protected TextInputEditText et_title;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        try {
            account = (Account) getArguments().getSerializable("account");
        }
        catch (Exception e) {

        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {

        et_title = getView().findViewById(R.id.et_title);
        if (account != null)
            et_title.setText(account.title);

        Button button = getView().findViewById(R.id.but_save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (account == null){
                    // fetch data and create account object
                    account = new Account(et_title.getText().toString());
                }
                else {
                    // get new title and update account object
                    account.title = et_title.getText().toString();
                }

                //save to db
                new CreateUpdateAccountAsyncTask(requireActivity(), AccountFragment.this, account).execute();
            }
        });
    }
}
