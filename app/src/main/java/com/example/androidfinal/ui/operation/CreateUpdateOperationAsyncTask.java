package com.example.androidfinal.ui.operation;

import android.os.AsyncTask;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.data.entities.Operation;

import java.lang.ref.WeakReference;

import androidx.fragment.app.FragmentActivity;
import androidx.navigation.fragment.NavHostFragment;

public class CreateUpdateOperationAsyncTask extends AsyncTask<Void,Void,Boolean> {

    private WeakReference<FragmentActivity> activityReference;
    private WeakReference<OperationFragment> fragmentReference;
    private Operation operation;

    CreateUpdateOperationAsyncTask(FragmentActivity context, OperationFragment fragment, Operation operation) {
        activityReference = new WeakReference<>(context);
        fragmentReference = new WeakReference<>(fragment);
        this.operation = operation;
    }

    // doInBackground methods runs on a worker thread
    @Override
    protected Boolean doInBackground(Void... voids) {
        ((MainActivity)activityReference.get()).appDatabase.operationDao().insertAll(operation);
        return true;
    }

    // onPostExecute runs on main thread
    @Override
    protected void onPostExecute(Boolean bool) {
        NavHostFragment.findNavController(fragmentReference.get()).navigateUp();
    }
}
