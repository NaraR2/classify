package com.example.androidfinal.ui.category;

import android.os.AsyncTask;

import com.example.androidfinal.MainActivity;
import com.example.androidfinal.data.entities.Category;

import java.lang.ref.WeakReference;

import androidx.fragment.app.FragmentActivity;
import androidx.navigation.fragment.NavHostFragment;

public class CreateUpdateCategoryAsyncTask extends AsyncTask<Void,Void,Boolean> {

    private WeakReference<FragmentActivity> activityReference;
    private WeakReference<CategoryFragment> fragmentReference;
    private Category category;

   CreateUpdateCategoryAsyncTask(FragmentActivity context, CategoryFragment fragment, Category category) {
        activityReference = new WeakReference<>(context);
        fragmentReference = new WeakReference<>(fragment);
        this.category = category;
    }

    // doInBackground methods runs on a worker thread
    @Override
    protected Boolean doInBackground(Void... voids) {
        ((MainActivity)activityReference.get()).appDatabase.categoryDao().insertAll(category);
        return true;
    }

    // onPostExecute runs on main thread
    @Override
    protected void onPostExecute(Boolean bool) {
        NavHostFragment.findNavController(fragmentReference.get()).navigateUp();
    }
}
